# -*- coding: utf-8 -*-
import os
from search_me.storage import SafeStorage
from search_me.tests.fixtures import empty_df


def test_save_salt():
    file = "test_salt_1"
    salt = SafeStorage.save_salt(file)
    os.remove(file)
    assert isinstance(salt, bytes)


def test_load_salt():
    file = "test_salt_2"
    salt = SafeStorage.save_salt(file)
    salt_loaded = SafeStorage.load_salt(file)
    os.remove(file)
    assert salt == salt_loaded


def test_load_key():
    assert SafeStorage.load_key(b"password", b"salt") is not None


def test_save(empty_df):
    s1, p1, df1 = "s1", "p1", "df1"
    with SafeStorage.save(s1, p1) as saver:
        saver.send((df1, empty_df))
    for f in (s1, p1, df1):
        os.remove(f)


def test_load(empty_df):
    s2, p2, df2 = "s2", "p2", "df2"
    with SafeStorage.save(s2, p2) as saver:
        saver.send((df2, empty_df))
    with SafeStorage.load(s2, p2) as loader:
        assert loader.send(df2) == empty_df
    for f in (s2, p2, df2):
        os.remove(f)
