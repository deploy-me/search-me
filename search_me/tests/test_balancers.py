# -*- coding: utf-8 -*-
import pytest
from search_me.balancers import RR, WRR, DWRR
from search_me.tests.fixtures import rr_full_data, wrr_full_data, rr_full, wrr_full, dwrr_full, iter_size


def test_rr_empty():
    rr = RR()
    with pytest.raises(StopIteration) as exc:
        next(rr)


def test_rr_full_iter(rr_full, rr_full_data):
    assert next(rr_full) == rr_full_data[0]


def test_rr_full_count(rr_full, iter_size):
    for _ in range(iter_size):
        next(rr_full)
    assert sum(rr_full.calls.values()) == iter_size


def test_wrr_empty():
    with pytest.raises(ValueError) as exc:
        wrr = WRR()


def test_wrr_full_iter(wrr_full, wrr_full_data):
    assert next(wrr_full) == wrr_full_data[1][0]


def test_wrr_full_count(wrr_full, iter_size):
    for _ in range(iter_size):
        next(wrr_full)
    assert sum(wrr_full.calls.values()) == iter_size


def test_dwrr_empty():
    dwrr = DWRR()
    with pytest.raises(ZeroDivisionError) as exc:
        next(dwrr)
        

def test_dwrr_full_iter(dwrr_full, rr_full_data):
    assert next(dwrr_full) == rr_full_data[0]


def test_dwrr_full_iter_random(dwrr_full, rr_full_data):
    dwrr_full.recalc_weights(rr_full_data[0], -1)
    dwrr_full.recalc_weights(rr_full_data[1], 1)
    dwrr_full.recalc_weights(rr_full_data[-1], -1)
    assert next(dwrr_full) is not None


def test_dwrr_full_count(dwrr_full, iter_size):
    for _ in range(iter_size):
        next(dwrr_full)
    assert sum(dwrr_full.calls.values()) == iter_size
