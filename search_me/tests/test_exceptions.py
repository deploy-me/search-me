# -*- coding: utf-8 -*-
import pytest
from search_me import (
    SearchEngineRequestError, SearchEngineAccessError, SearchEngineFormatError, SearchEngineParamsError
)


def test_sere():
    domain, status_code = "ru", 500
    with pytest.raises(SearchEngineRequestError) as exc:
        raise SearchEngineRequestError(domain, status_code)
        assert exc.domain == domain
        assert exc.status_code == status_code
        assert str(exc) == f"Bad request: {domain}. Status code: {status_code}"


def test_seae():
    with pytest.raises(SearchEngineAccessError) as exc:
        raise SearchEngineAccessError()
        assert str(exc) == "No access to attr"


def test_sefe():
    frmt = "xml"
    with pytest.raises(SearchEngineFormatError) as exc:
        raise SearchEngineFormatError(frmt)
        assert exc.format == frmt
        assert str(exc) == f"Can't handle format {frmt}"


def test_sepe():
    params = "p1", "p2"
    with pytest.raises(SearchEngineParamsError) as exc:
        raise SearchEngineParamsError(params)
        assert exc.params == params
        assert str(exc) == f"Set up params {params}"
