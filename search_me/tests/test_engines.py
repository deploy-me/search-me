# -*- coding: utf-8 -*-
import pytest
from search_me.exceptions import SearchEngineAccessError, SearchEngineParamsError
from search_me.models import SearchResult
from search_me.tests.fixtures import (
    client_session, e_google, e_bing, e_brave, e_mojeek, e_moose, e_yahoo, e_searx, e_etools,
    q1, q2, q3, q4
)


@pytest.mark.asyncio
async def test_google_result(e_google, client_session, q3):
    for t in (await e_google.search(session=client_session, q=q3, num=10)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_bing_result(e_bing, client_session, q4):
    for t in (await e_bing.search(session=client_session, q=q4)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_brave_result(e_brave, client_session, q1):
    for t in (await e_brave.search(session=client_session, q=q1)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_mojeek_result(e_mojeek, client_session, q2):
    for t in (await e_mojeek.search(session=client_session, q=q2)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_moose_result(e_moose, client_session, q1):
    for t in (await e_moose.search(session=client_session, q=q1)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_yahoo_result(e_yahoo, client_session, q3):
    for t in (await e_yahoo.search(session=client_session, q=q3)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_searx_result(e_searx, client_session, q4):
    for t in (await e_searx.search(session=client_session, q=q4)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_etools_result(e_etools, client_session, q2):
    for t in (await e_etools.search(session=client_session, q=q2)):
        assert isinstance(t, SearchResult)


@pytest.mark.asyncio
async def test_raw(e_google, client_session, q2):
    for t in (await e_google.search(session=client_session, q=q2, return_raw=True)):
        assert isinstance(t, tuple)


@pytest.mark.asyncio
async def test_check_params(e_google, client_session):
    with pytest.raises(SearchEngineParamsError) as exc:
        _ = await e_google.search(session=client_session)


def test_name(e_google):
    assert e_google.name == "Google"


def test_setter(e_bing):
    with pytest.raises(SearchEngineAccessError) as exc:
        del e_bing.name, e_bing.domains, e_bing.languages, e_bing.regex, e_bing.output_format


def test_deleter(e_brave):
    with pytest.raises(SearchEngineAccessError) as exc:
        e_brave.name, e_brave.domains, e_brave.languages, e_brave.regex, e_brave.output_format = (
            None, None, None, None, None
            )


def test_is_html(e_searx):
    assert not e_searx.is_html
