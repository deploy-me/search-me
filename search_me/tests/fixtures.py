# -*- coding: utf-8 -*-
import pytest
from aiohttp import ClientSession, ClientTimeout
from pandas import DataFrame
from search_me.balancers import RR, WRR, DWRR
from search_me.engines import Google, Bing, Brave, Mojeek, Moose, Yahoo, Searx, Etools


@pytest.yield_fixture
async def client_session(autouse=True):
    async with ClientSession(
        timeout=ClientTimeout(total=360),
        headers={"User-Agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0"}
        ) as session:
        yield session


@pytest.fixture
async def rr_full_data():
    return ["A", "B", "C", "D", "E", "F"]


@pytest.fixture
async def wrr_full_data():
    return [("A", 10), ("B", 100), ("C", 60), ("D", 50), ("E", 70), ("F", 100)]


@pytest.fixture
async def rr_full(rr_full_data):
    return RR(*rr_full_data)


@pytest.fixture
async def wrr_full(wrr_full_data):
    return WRR(*wrr_full_data)


@pytest.fixture
async def dwrr_full(rr_full_data):
    return DWRR(*rr_full_data)


@pytest.fixture
async def iter_size():
    return 100


@pytest.fixture
def e_google():
    return Google()


@pytest.fixture
def e_bing():
    return Bing(delay=1)


@pytest.fixture
def e_brave():
    return Brave()


@pytest.fixture
def e_mojeek():
    return Mojeek()


@pytest.fixture
def e_moose():
    return Moose()


@pytest.fixture
def e_yahoo():
    return Yahoo()


@pytest.fixture
def e_searx():
    return Searx(retry=10)


@pytest.fixture
def e_etools():
    return Etools()


@pytest.fixture
def q1():
    return "aioboy"


@pytest.fixture
def q2():
    return "0x0007ee"


@pytest.fixture
def q3():
    return "社會信用體系"


@pytest.fixture
def q4():
    return "Виктор Суворов — Аквариум"


@pytest.fixture
def empty_df():
    return DataFrame()
