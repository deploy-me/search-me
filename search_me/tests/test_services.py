# -*- coding: utf-8 -*-
import pytest
from search_me.exceptions import ApiError
from search_me.services import UserAgents, Dorks


def test_smoke():
    ua = UserAgents()
    for _ in range(100):
        assert isinstance(ua.get(), str)


def test_dorks_empty_str():
    with pytest.raises(ApiError) as exc:
        dorks = Dorks(api_key="")


def test_dorks_empty_tuple():
    with pytest.raises(ApiError) as exc:
        dorks = Dorks(api_key=("", ""))
