# -*- coding: utf-8 -*-
import pytest
from search_me.exceptions import ApiError
from search_me.osint import Domain


def test_osint_error():
    with pytest.raises(ApiError) as exc:
        dorks = Domain(api_key="")
    with pytest.raises(ApiError) as exc:
        dorks = Domain(api_key=("", ""))
