# -*- coding: utf-8 -*-
import pytest
from search_me.models import SearchResult


def test_empty():
    with pytest.raises(TypeError) as exc:
        SearchResult()

def test_to_dict():
    data = {"q": "q", "rating": 0, "uri": "uri", "title": "title", "source": "source"}
    assert SearchResult(**data).to_dict() == data
