[![SEARCH-ME](img/logo.gif)](https://bit.ly/search--me)

[![Version](https://img.shields.io/pypi/v/search-me.svg?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![License](https://img.shields.io/pypi/l/search-me.svg?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![Python](https://img.shields.io/pypi/pyversions/search-me.svg?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![Status](https://img.shields.io/pypi/status/search-me.svg?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![Format](https://img.shields.io/pypi/format/search-me.svg?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![Wheel](https://img.shields.io/pypi/wheel/search-me.svg?style=flat-square&logo=appveyor&color=red)](https://pypi.org/project/search-me)
[![Build](https://img.shields.io/bitbucket/pipelines/deploy-me/search-me/master?style=flat-square&logo=appveyor)](https://pypi.org/project/search-me)
[![Coverage](img/coverage.svg)](https://pypi.org/project/search-me)
[![Downloads](https://static.pepy.tech/personalized-badge/search-me?period=total&units=international_system&left_color=black&right_color=blue&left_text=Downloads)](https://pepy.tech/project/search-me)

# SEARCH-ME

Search in Google, Bing, Brave, Mojeek, Moose, Yahoo, Searx. See more in [documentation](https://deploy-me.bitbucket.io/search-me/index.html)

## INSTALL

```bash
pip install search-me
```

## USAGE

```python
import asyncio
import logging
import itertools
import aiohttp
from search_me import Google, Bing, Brave, Mojeek, Moose, Yahoo, Searx


logging.basicConfig(level=logging.DEBUG)

s, b, g = Searx(retry=10), Brave(), Google()


async def main():
    async with aiohttp.ClientSession(
        timeout=aiohttp.ClientTimeout(total=30),
        headers={"User-Agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0"}
        ) as session:
            results = await asyncio.gather(
                s.search(session=session, q="社會信用體系"),
                b.search(session=session, q="python 3.12"),
                g.search(session=session, q="0x0007ee")
                )
            for x in itertools.chain(*results):
                print(x)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```
